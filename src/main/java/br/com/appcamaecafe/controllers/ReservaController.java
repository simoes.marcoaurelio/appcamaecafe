package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.models.DTOs.ReservaDTO;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.services.ReservaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/reserva")
public class ReservaController {
    @Autowired
    private ReservaService reservaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Reserva incluirPropriedade(@RequestBody @Valid ReservaDTO reservaDTO) {
        return reservaService.salvarReserva(reservaDTO);
    }

    @GetMapping({"/propriedade/{id}"})
    public Iterable<Reserva> buscarReservasPropriedades(@PathVariable(name = "id") int id) {
        return reservaService.buscarReservaPorPropriedade(id);
    }

    @GetMapping({"/hospede/{id}"})
    public Iterable<Reserva> buscarReservasHospedes(@PathVariable(name = "id") int id) {
        return reservaService.buscarReservaPorHospede(id);
    }
}
