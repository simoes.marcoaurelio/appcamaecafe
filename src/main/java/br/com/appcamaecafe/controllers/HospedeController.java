package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.services.HospedeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/hospedes")
public class HospedeController {

    @Autowired
    private HospedeService hospedeService;

    @PostMapping
    public Hospede incluirHospede(@RequestBody Hospede hospede){
        return hospedeService.incluirHospede(hospede);
    }

    @GetMapping("/{id}")
    public Hospede buscarPorId(@PathVariable int id) throws ResponseStatusException{
        try{
            Hospede hospede = hospedeService.buscarHospedePorId(id);
            return hospede;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
