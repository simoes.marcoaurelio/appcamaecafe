package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.services.PropriedadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/propriedades")
public class PropriedadeController {

    @Autowired
    private PropriedadeService propriedadeService;

    @PostMapping("/{idProprietario}")
    @ResponseStatus(HttpStatus.CREATED)
    public Propriedade incluirPropriedade(
            @PathVariable(name = "idProprietario") int idProprietario,
            @RequestBody @Valid Propriedade propriedade) {
        return propriedadeService.incluirPropriedade(propriedade, idProprietario);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public Propriedade buscarPropriedadePorId(@PathVariable(name = "id") int id) {
        try {
            Propriedade propriedade = propriedadeService.buscarPropriedadePorId(id);
            return propriedade;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public Iterable<Propriedade> buscarPropriedadesPorCidade(
            @RequestParam(name = "cidade", required = true) String cidade) {
        try {
            Iterable<Propriedade> propriedades = propriedadeService.buscarPropriedadesPorCidade(cidade);
            return propriedades;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
