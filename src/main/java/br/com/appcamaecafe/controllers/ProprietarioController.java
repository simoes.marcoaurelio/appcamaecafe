package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Proprietario;
import br.com.appcamaecafe.services.ProprietarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/proprietario")
public class ProprietarioController {

    @Autowired
    private ProprietarioService proprietarioService;

    //               "cpf": "429.187.590-82",
//               "nome": "Capitão América",
//               "dataDeNascimento": "1990-03-03",
//               "email": "teste1@teste.com",
//               "telefone": 12345678,
//               "genero": "MASCULINO"
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Proprietario registrarProprietario(@RequestBody @Valid Proprietario proprietario){
        Proprietario proprietarioObjeto = proprietarioService.salvarProprietario(proprietario);
        return proprietarioObjeto;
    }

    @PutMapping("/{idProprietario}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void incluirPropriedade(@PathVariable(name="idProprietario") int idProprietario
            ,@RequestBody Propriedade propriedade){
        try{
            proprietarioService.incluirPropriedade(idProprietario, propriedade);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Proprietario buscarProprietarioPorId(@PathVariable(name="id") int id){
        try{
            Proprietario proprietario = proprietarioService.buscarProprietarioPorId(id);
            return proprietario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public Proprietario buscarProprietarioPorCpf(@RequestParam(name="cpf") String cpf){
        try{
            Proprietario proprietario = proprietarioService.buscarProprietarioPorCpf(cpf);
            return proprietario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
