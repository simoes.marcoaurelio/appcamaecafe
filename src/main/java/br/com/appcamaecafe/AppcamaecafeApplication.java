package br.com.appcamaecafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppcamaecafeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppcamaecafeApplication.class, args);
	}

}
