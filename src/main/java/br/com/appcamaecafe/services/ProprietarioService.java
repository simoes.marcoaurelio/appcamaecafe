package br.com.appcamaecafe.services;

import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Proprietario;
import br.com.appcamaecafe.repositories.ProprietarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProprietarioService {

    @Autowired
    private ProprietarioRepository proprietarioRepository;

    public Proprietario salvarProprietario(Proprietario proprietario){
        Proprietario proprietarioObjeto = proprietarioRepository.save(proprietario);
        return proprietarioObjeto;
    }

    public Proprietario atualizarProprietario(int id, Proprietario proprietario){
        if (proprietarioRepository.existsById(id)){
            proprietario.setId(id);
            Proprietario proprietarioObjeto = proprietarioRepository.save(proprietario);
            return proprietarioObjeto;
        }
        throw new RuntimeException("Proprietario não encontrado, ID informado: " + id);
    }

    public void excluirProprietario(int id){
        if (proprietarioRepository.existsById(id)){
            proprietarioRepository.deleteById(id);
        }else{
            throw new RuntimeException("Proprietario não encontrado, ID informado: " + id);
        }
    }

    public Proprietario buscarProprietarioPorId(int id){
        Optional<Proprietario> optionalProprietario = proprietarioRepository.findById(id);
        if (optionalProprietario.isPresent()) {
            return optionalProprietario.get();
        }
        throw new RuntimeException("Proprietário não encontrado, ID informado: " + id);
    }

    public void incluirPropriedade(int id, Propriedade propriedade){
        Proprietario proprietario = buscarProprietarioPorId(id);
        List<Propriedade> propriedades = proprietario.getPropriedades();
        propriedades.add(propriedade);
        proprietario.setPropriedades(propriedades);
        proprietarioRepository.save(proprietario);
    }

    public Proprietario buscarProprietarioPorCpf(String Cpf){
        Optional<Proprietario> optionalProprietario = proprietarioRepository.findByCpf(Cpf);
        if (optionalProprietario.isPresent()) {
            return optionalProprietario.get();
        }
        throw new RuntimeException("Proprietário não encontrado, ID informado: " + Cpf);
    }
}

