package br.com.appcamaecafe.services;


import br.com.appcamaecafe.models.DTOs.ReservaDTO;
import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.repositories.ReservaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.UUID;

@Service
public class ReservaService {
    @Autowired
    ReservaRepository reservaRepository;
    @Autowired
    PropriedadeService propriedadeService;
    @Autowired
    HospedeService hospedeService;

    public Reserva salvarReserva(ReservaDTO reserva) {

        Propriedade propriedade = propriedadeService.buscarPropriedadePorId(reserva.getIdPropriedade());
        Hospede hospede = hospedeService.buscarHospedePorId(reserva.getIdHospede());
        Period periodo;

        Iterable<Reserva> reservas = buscarReservaPorPropriedade(propriedade);
        for (Reserva reservaBusca : reservas) {
            if (reservaBusca.getDataInicio().isBefore(reserva.getDataFim())) {
                throw new RuntimeException("Data ja reservada");
            } else if (reservaBusca.getDataFim().isAfter(reserva.getDataInicio()))
                throw new RuntimeException("Data ja reservada");
        }

        Reserva criarReserva = new Reserva();
        criarReserva.setPropriedade(propriedade);
        criarReserva.setHospede(hospede);
        criarReserva.setDataFim(reserva.getDataFim());
        criarReserva.setDataFim(reserva.getDataFim());


        periodo = Period.between(reserva.getDataInicio(), reserva.getDataFim());
        if (periodo.getDays() <= 0)
            throw new RuntimeException("Data final menor que data inicial");
        criarReserva.setValorDaReserva(propriedade.getValorDiaria() * periodo.getDays());

        reservaRepository.save(criarReserva);
        return criarReserva;
    }

    public Iterable<Reserva> buscarReservaPorHospede(Hospede hospede) {

        Iterable<Reserva> reservas = reservaRepository.findAllByHospede(hospede);
        return reservas;
    }

    public Iterable<Reserva> buscarReservaPorPropriedade(Propriedade propriedade) {

        Iterable<Reserva> reservas = reservaRepository.findAllByPropriedade(propriedade);
        return reservas;
    }

    public Iterable<Reserva> buscarReservaPorHospede(int idHospede) {
        Hospede hospede = hospedeService.buscarHospedePorId(idHospede);
        Iterable<Reserva> reservas = reservaRepository.findAllByHospede(hospede);
        return reservas;
    }

    public Iterable<Reserva> buscarReservaPorPropriedade(int idPropriedade) {
        Propriedade propriedade = propriedadeService.buscarPropriedadePorId(idPropriedade);
        Iterable<Reserva> reservas = reservaRepository.findAllByPropriedade(propriedade);
        return reservas;
    }


    public Reserva atualizarReserva(UUID id, Reserva reserva) {
        return reserva;
    }

}
