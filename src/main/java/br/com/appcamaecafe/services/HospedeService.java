package br.com.appcamaecafe.services;

import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.repositories.HospedeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class HospedeService {
    @Autowired
    private HospedeRepository hospedeRepository;

    public Hospede salvarHospede(Hospede hospede){
        Hospede hospedeObjeto = hospedeRepository.save(hospede);
        return hospedeObjeto;
    }

    public Hospede incluirHospede(Hospede hospede){
        return salvarHospede(hospede);
    }

    public Iterable<Hospede> buscarTodos(){
        Iterable<Hospede> hospedes = hospedeRepository.findAll();
        return hospedes;
    }

    public Hospede buscarHospedePorId(int id) {
        Optional<Hospede> optionalHospede = hospedeRepository.findById(id);
        if(optionalHospede.isPresent()){
            return optionalHospede.get();
        }else{
            throw new RuntimeException("Não existe hospede com o Id informado.");
        }
    }

    public Hospede atualizarHospede(int id, Hospede hospede) throws RuntimeException{
        if(hospedeRepository.existsById(id)){
            hospede.setId(id);
            Hospede hospedeObjeto = salvarHospede(hospede);
            return hospedeObjeto;
        }else{
            throw new RuntimeException("Não existe hospede com o Id informado.");
        }
    }

    public void excluirHospede(int id) throws RuntimeException{
        if(hospedeRepository.existsById(id)){
            hospedeRepository.deleteById(id);
        }else{
            throw new RuntimeException("Não existe hospede com o Id informado.");
        }
    }
}
