package br.com.appcamaecafe.services;

import br.com.appcamaecafe.models.Proprietario;
import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.repositories.PropriedadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PropriedadeService {

    @Autowired
    private PropriedadeRepository propriedadeRepository;

    @Autowired
    private ProprietarioService proprietarioService;


    //Incluir nova propriedade
    public Propriedade incluirPropriedade(Propriedade propriedade, int idProprietario) {
        try {
            Proprietario proprietario = proprietarioService.buscarProprietarioPorId(idProprietario);
            propriedade.setProprietario(proprietario);

            Propriedade propriedadeObjeto = propriedadeRepository.save(propriedade);
            return propriedadeObjeto;

        } catch (RuntimeException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }


    // Buscar propriedade por ID
    public Propriedade buscarPropriedadePorId(int id) {
        Optional<Propriedade> optionalPropriedade = propriedadeRepository.findById(id);
        if ((optionalPropriedade.isPresent())) {
            return optionalPropriedade.get();
        }
        throw new RuntimeException("A propriedade não foi encontrada");
    }


    // Buscar propriedade por cidade
    public Iterable<Propriedade> buscarPropriedadesPorCidade(String cidade) {
        Iterable<Propriedade> propriedades = propriedadeRepository.findAllByCidade(cidade);

        int contador = 0;
        for (Propriedade propriedadeObjeto : propriedades)
            contador++;

        if (contador == 0) {
            throw new RuntimeException("Não existem propriedades na cidade selecionada");
        }
        return propriedades;

    }
}
