package br.com.appcamaecafe.repositories;

import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Reserva;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ReservaRepository extends CrudRepository<Reserva, Integer> {
    Iterable<Reserva> findAllByHospede(Hospede hospede);
    Iterable<Reserva> findAllByPropriedade(Propriedade propriedade);
}
