package br.com.appcamaecafe.repositories;

import br.com.appcamaecafe.models.Proprietario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProprietarioRepository extends CrudRepository<Proprietario, Integer> {
    Optional<Proprietario> findById(int id);
    Optional<Proprietario> findByCpf(String Cpf);
}
