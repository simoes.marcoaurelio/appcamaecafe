package br.com.appcamaecafe.repositories;

import br.com.appcamaecafe.models.Propriedade;
import org.springframework.data.repository.CrudRepository;

public interface PropriedadeRepository extends CrudRepository<Propriedade, Integer> {
    Iterable<Propriedade> findAllByCidade(String cidade);
}
