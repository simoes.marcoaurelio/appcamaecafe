package br.com.appcamaecafe.repositories;

import br.com.appcamaecafe.models.Hospede;
import org.springframework.data.repository.CrudRepository;


public interface HospedeRepository extends CrudRepository<Hospede, Integer> {
}
