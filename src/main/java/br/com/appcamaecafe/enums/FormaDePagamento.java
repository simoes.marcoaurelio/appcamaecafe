package br.com.appcamaecafe.enums;

public enum FormaDePagamento {
    CARTAO,
    BOLETO,
    CARTEIRA
}
