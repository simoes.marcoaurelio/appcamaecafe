package br.com.appcamaecafe.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
public class Reserva {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(cascade = CascadeType.ALL)
    private Hospede hospede;

    @ManyToOne(cascade = CascadeType.ALL)
    private Propriedade propriedade;

    private LocalDate dataInicio;
    private LocalDate dataFim;
    private Double valorDaReserva;

    public Reserva(UUID id, Hospede hospede, Propriedade propriedade, LocalDate dataInicio, LocalDate dataFim
                    , Double valorDaReserva) {
        this.id = id;
        this.hospede = hospede;
        this.propriedade = propriedade;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.valorDaReserva = valorDaReserva;
    }

    public Reserva() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public Hospede getHospede() {
        return hospede;
    }

    public void setHospede(Hospede hospede) {
        this.hospede = hospede;
    }

    public Propriedade getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Propriedade propriedade) {
        this.propriedade = propriedade;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public Double getValorDaReserva() {
        return valorDaReserva;
    }

    public void setValorDaReserva(Double valorDaReserva) {
        this.valorDaReserva = valorDaReserva;
    }

}
