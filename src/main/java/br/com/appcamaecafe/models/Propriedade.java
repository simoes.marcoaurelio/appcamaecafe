package br.com.appcamaecafe.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Propriedade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Insira uma descrição para sua propriedade")
    private String descricao;

    @NotNull(message = "Insira o endereço válido da sua propriedade")
    private String logradouro;

    @Min(value = 1, message = "O número da propriedade precisa ser preenchido")
    private int numero;

    private String complemento;

    @Digits(integer = 8, fraction = 0, message = "Digite o CEP válido da sua propriedade")
    private int cep;

    @NotNull(message = "Preencha corretamente a cidade de sua propriedade")
    private String cidade;

//    @Pattern(regexp = "\\d{6}", message = "Digite o estado (no formato XX) de sua propriedade")
    private String uf;

    @DecimalMin(value = "0.1", message = "Você precisa definir um valor de aluguel para a diária")
    private double valorDiaria;

    @ManyToOne(cascade = CascadeType.ALL)
    private Proprietario proprietario;


    // Construtor vazio
    public Propriedade() {
    }


    // Getters & Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public int getCep() {
        return cep;
    }

    public void setCep(int cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public double getValorDiaria() {
        return valorDiaria;
    }

    public void setValorDiaria(double valorDiaria) {
        this.valorDiaria = valorDiaria;
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }
}