package br.com.appcamaecafe.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class Proprietario extends Cliente{

    private int contaDeRecebimento;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Propriedade> propriedades;

    public Proprietario(){
    }

    public int getContaDeRecebimento() {
        return contaDeRecebimento;
    }

    public void setContaDeRecebimento(int contaDeRecebimento) {
        this.contaDeRecebimento = contaDeRecebimento;
    }

    public List<Propriedade> getPropriedades() {
        return propriedades;
    }

    public void setPropriedades(List<Propriedade> propriedades) {
        this.propriedades = propriedades;
    }
}
