package br.com.appcamaecafe.models;

import br.com.appcamaecafe.enums.FormaDePagamento;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Hospede extends Cliente {
    private FormaDePagamento formaDePagamento;

    @OneToMany
    private List<Reserva> reservas;

    public Hospede() {
    }

    public FormaDePagamento getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(FormaDePagamento formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }


    public List<Reserva> getReservas() {
        return reservas;
    }

    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }

}