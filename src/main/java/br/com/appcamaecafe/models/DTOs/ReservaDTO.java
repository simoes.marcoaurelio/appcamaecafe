package br.com.appcamaecafe.models.DTOs;

import java.time.LocalDate;


public class ReservaDTO {
    private int idHospede;
    private int idPropriedade;
    private LocalDate dataInicio;
    private LocalDate dataFim;

    public ReservaDTO(int idHospede, int idPropriedade, LocalDate dataInicio, LocalDate dataFim) {
        this.idHospede = idHospede;
        this.idPropriedade = idPropriedade;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }

    public ReservaDTO() {
    }

    public int getIdHospede() {
        return idHospede;
    }

    public void setIdHospede(int idHospede) {
        this.idHospede = idHospede;
    }

    public int getIdPropriedade() {
        return idPropriedade;
    }

    public void setIdPropriedade(int idPropriedade) {
        this.idPropriedade = idPropriedade;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }
}
