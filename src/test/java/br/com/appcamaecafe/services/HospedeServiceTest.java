package br.com.appcamaecafe.services;

import br.com.appcamaecafe.enums.FormaDePagamento;
import br.com.appcamaecafe.enums.Genero;
import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.repositories.HospedeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class HospedeServiceTest {
    @MockBean
    private HospedeRepository hospedeRepository;
    @Autowired
    private HospedeService hospedeService;

    Hospede hospede;
    List<Reserva> reservas;

    @BeforeEach
    public void setUp(){
        reservas = new ArrayList<Reserva>();
        hospede = new Hospede();
        hospede.setId(1);
        hospede.setFormaDePagamento(FormaDePagamento.CARTAO);
        hospede.setReservas(reservas);
        hospede.setCpf("76748533066");
        hospede.setDataDeNascimento(LocalDate.parse("2000-01-01"));
        hospede.setEmail("bob@bobmail.com");
        hospede.setNome("Bob");
        hospede.setGenero(Genero.MASCULINO);
        hospede.setTelefone(123456789);
    }

    @Test
    public void testarSalvarHospede(){
        Mockito.when(hospedeRepository.save(hospede)).thenReturn(hospede);
        Hospede hospedeObjeto = hospedeService.salvarHospede(hospede);
        Assertions.assertEquals(hospede, hospedeObjeto);
    }

    @Test
    public void testarIncluirHospede(){
        Mockito.when(hospedeService.salvarHospede(hospede)).thenReturn(hospede);
        Hospede hospedeObjeto = hospedeService.incluirHospede(hospede);
        Assertions.assertEquals(hospedeObjeto, hospede);
    }

    @Test
    public void testarBuscarHospedes(){
        Iterable<Hospede> hospedes = Arrays.asList(hospede);
        Mockito.when(hospedeRepository.findAll()).thenReturn(hospedes);
        Iterable<Hospede> hospedesObjeto = hospedeService.buscarTodos();
        Assertions.assertEquals(hospedes, hospedesObjeto);
    }

    @Test
    public void testarBuscarHospedePorId(){
        Mockito.when(hospedeRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(hospede));
        Hospede hospedeObjeto = hospedeService.buscarHospedePorId(1);
        Assertions.assertEquals(hospede, hospedeObjeto);
    }

    @Test
    public void testarBuscarHospedePorIdInexistente(){
        Mockito.when(hospedeRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {hospedeService.buscarHospedePorId(404);});
    }

    @Test
    public void testarAtualizarHospede(){
        Mockito.when(hospedeService.salvarHospede(hospede)).thenReturn(hospede);
        Mockito.when(hospedeRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Hospede hospedeObjeto = hospedeService.atualizarHospede(1,hospede);
        Assertions.assertEquals(hospedeObjeto.getId(), hospede.getId());
    }

    @Test
    public void testarAtualizarHospedeInexistente(){
        Mockito.when(hospedeRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {hospedeService.atualizarHospede(404,hospede);});
    }

    @Test
    public void testarExcluirHospede(){
        Mockito.when(hospedeRepository.existsById(Mockito.anyInt())).thenReturn(true);
        hospedeService.excluirHospede(1);
        hospedeService.excluirHospede(2);
        Mockito.verify(hospedeRepository, Mockito.times(2)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarExcluirHospedeInexistente(){
        Mockito.when(hospedeRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {hospedeService.excluirHospede(404);});
    }

}
