package br.com.appcamaecafe.services;

import br.com.appcamaecafe.enums.Genero;
import br.com.appcamaecafe.models.Proprietario;
import br.com.appcamaecafe.repositories.ProprietarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProprietarioServiceTest {

    @MockBean
    private ProprietarioRepository proprietarioRespository;

    @Autowired
    private ProprietarioService proprietarioService;

    Proprietario proprietario;

    @BeforeEach
    public void inicializadorSetup(){
        proprietario = new Proprietario();
        proprietario.setCpf("29473150814");
        proprietario.setDataDeNascimento(LocalDate.of(1980, 9, 12));
        proprietario.setEmail("teste@teste.com.br");
        proprietario.setGenero(Genero.MASCULINO);
    }

    @Test
    public void testarBuscarPorCpf() {
        Optional<Proprietario> optionalProprietario = Optional.of(this.proprietario);
        Mockito.when(proprietarioRespository.findByCpf(this.proprietario.getCpf())).thenReturn(optionalProprietario);
        proprietarioService.buscarProprietarioPorCpf(proprietario.getCpf());
        Assertions.assertEquals(optionalProprietario, optionalProprietario);
    }

    @Test
    public void testarBuscarPorCpfNaoEncontrado() {
        Optional<Proprietario> optionalProprietario;
        Mockito.when(proprietarioRespository.findByCpf(this.proprietario.getCpf())).thenReturn(null);
        Assertions.assertThrows(RuntimeException.class, () -> {
            proprietarioService.buscarProprietarioPorCpf(proprietario.getCpf());});
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Proprietario> optionalProprietario = Optional.of(this.proprietario);
        Mockito.when(proprietarioRespository.findById(proprietario.getId())).thenReturn(optionalProprietario);
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado() {
        Optional<Proprietario> optionalProprietario;
        Mockito.when(proprietarioRespository.findById(this.proprietario.getId())).thenReturn(null);
        Assertions.assertThrows(RuntimeException.class, () -> {
            proprietarioService.buscarProprietarioPorId(proprietario.getId());});
    }


}
