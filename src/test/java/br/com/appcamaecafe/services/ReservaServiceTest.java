package br.com.appcamaecafe.services;

import br.com.appcamaecafe.models.DTOs.ReservaDTO;
import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.repositories.ReservaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootTest
public class ReservaServiceTest {
    @MockBean
    private PropriedadeService propriedadeService;
    @Autowired
    private ReservaService reservaService;
    @MockBean
    private HospedeService hospedeService;
    @MockBean
    private ReservaRepository reservaRepository;

    Hospede hospede = new Hospede();
    Propriedade propriedade = new Propriedade();

    @BeforeEach
    public void Setup() {
        propriedade.setValorDiaria(200.20);
        hospede.setId(23);
    }

    @Test
    public void testarCriarReserva() {

        Reserva reserva = new Reserva();

        reserva.setPropriedade(propriedade);
        reserva.setHospede(hospede);
        reserva.setDataInicio(LocalDate.parse("2020-09-08"));
        reserva.setDataFim(LocalDate.parse("2020-09-10"));
        reserva.setValorDaReserva(400.40);

        ReservaDTO reservaDTOTeste = new ReservaDTO();
        Hospede hospede1 = new Hospede();
        hospede1.setId(23);
        Propriedade propriedade2 = new Propriedade();
        propriedade2.setValorDiaria(200.20);

        reservaDTOTeste.setIdPropriedade(2);
        reservaDTOTeste.setIdHospede(2);
        reservaDTOTeste.setDataInicio(LocalDate.parse("2020-09-08"));
        reservaDTOTeste.setDataFim(LocalDate.parse("2020-09-10"));

        Mockito.when(propriedadeService.buscarPropriedadePorId(Mockito.anyInt())).thenReturn(propriedade2);
        Mockito.when(hospedeService.buscarHospedePorId(Mockito.anyInt())).thenReturn(hospede1);
        Mockito.when(reservaRepository.save(Mockito.any(Reserva.class))).thenReturn(reserva);
        Reserva reservaRetorno = reservaService.salvarReserva(reservaDTOTeste);
        Assertions.assertEquals(reservaRetorno.getValorDaReserva(), reserva.getValorDaReserva());
    }

    @Test
    public void testarReservaPeriodoJaReservado() {

        Reserva reserva = new Reserva();
        Hospede hospede1 = new Hospede();
        hospede1.setId(23);
        Propriedade propriedade2 = new Propriedade();
        propriedade2.setValorDiaria(200.20);


        reserva.setPropriedade(propriedade);
        reserva.setHospede(hospede);
        reserva.setDataInicio(LocalDate.parse("2020-09-08"));
        reserva.setDataFim(LocalDate.parse("2020-09-10"));
        reserva.setValorDaReserva(400.40);
        Iterable<Reserva> listaReservas = Arrays.asList(reserva);

        ReservaDTO reservaDTOTeste = new ReservaDTO();
        reservaDTOTeste.setIdPropriedade(2);
        reservaDTOTeste.setIdHospede(2);
        reservaDTOTeste.setDataInicio(LocalDate.parse("2020-09-05"));
        reservaDTOTeste.setDataFim(LocalDate.parse("2020-09-09"));

        Mockito.when(reservaService.buscarReservaPorPropriedade(Mockito.any(Propriedade.class)))
                .thenReturn(listaReservas);
        Mockito.when(propriedadeService.buscarPropriedadePorId(Mockito.anyInt())).thenReturn(propriedade2);
        Mockito.when(hospedeService.buscarHospedePorId(Mockito.anyInt())).thenReturn(hospede1);
        Assertions.assertThrows(RuntimeException.class, () -> {
            reservaService.salvarReserva(reservaDTOTeste);
        });
    }

}
