package br.com.appcamaecafe.services;

import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Proprietario;
import br.com.appcamaecafe.repositories.PropriedadeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class PropriedadeSeviceTest {

    @MockBean
    private PropriedadeRepository propriedadeRepository;

    @MockBean
    private ProprietarioService proprietarioService;

    @Autowired
    public PropriedadeService propriedadeService;

    Propriedade propriedade;
    Proprietario proprietario;

    @BeforeEach
    public void setup() {

        proprietario = new Proprietario();
        propriedade = new Propriedade();

        propriedade.setDescricao("Nova propriedade teste");
        propriedade.setLogradouro("Rua 1");
        propriedade.setNumero(1234);
        propriedade.setComplemento("12a");
        propriedade.setCep(12345678);
        propriedade.setCidade("Cidade Teste");
        propriedade.setUf("SP");
        propriedade.setValorDiaria(250.00);

        proprietario.setId(1);
    }

    @Test
    public void testarIncluirPropriedade() {

        Mockito.when(proprietarioService.buscarProprietarioPorId(Mockito.anyInt())).thenReturn(proprietario);
        Mockito.when(propriedadeRepository.save(propriedade)).thenReturn(propriedade);

        Propriedade propriedadeObjeto = propriedadeService.incluirPropriedade(propriedade, proprietario.getId());

        Assertions.assertEquals(propriedadeObjeto, propriedade);
    }

    @Test
    public void testarIncluirPropriedadeSemProprietario() {

        Mockito.when(proprietarioService.buscarProprietarioPorId(2)).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {
            propriedadeService.incluirPropriedade(propriedade, 2);});
    }

    @Test
    public void testarBuscarPropriedadePorId() {

        Optional<Propriedade> propriedadeOptional = Optional.of(propriedade);
        Mockito.when(propriedadeRepository.findById(Mockito.anyInt())).thenReturn(propriedadeOptional);

        Propriedade propriedadeObjeto = propriedadeService.buscarPropriedadePorId(1);

        Assertions.assertEquals(propriedadeObjeto, propriedade);
    }

    @Test
    public void testarBuscarPropriedadePorIdInexistente() {

        Optional<Propriedade> propriedadeOptional = Optional.empty();
        Mockito.when((propriedadeRepository.findById(Mockito.anyInt()))).thenReturn(propriedadeOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {propriedadeService.buscarPropriedadePorId(2);});
    }

    @Test
    public void testarBuscarPropriedadePorCidade() {
        Iterable<Propriedade> propriedades = Arrays.asList(propriedade);
        Mockito.when(propriedadeRepository.findAllByCidade(Mockito.anyString())).thenReturn(propriedades);

        Iterable<Propriedade> propriedadeIterable =
                propriedadeService.buscarPropriedadesPorCidade(propriedade.getCidade());

        Assertions.assertEquals(propriedadeIterable, propriedades);
    }

    @Test
    public void testarBuscarPropriedadePorCidadeNaoContemplada() {

        propriedade = new Propriedade();
        Iterable<Propriedade> propriedades = Arrays.asList(propriedade);

        Mockito.when(propriedadeRepository.findAllByCidade(Mockito.anyString())).thenReturn(propriedades);

        Assertions.assertThrows(RuntimeException.class, () -> {
            propriedadeService.buscarPropriedadesPorCidade(propriedade.getCidade());});
    }
}
