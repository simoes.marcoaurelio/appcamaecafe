package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.models.DTOs.ReservaDTO;
import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Propriedade;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.services.ReservaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.UUID;

@WebMvcTest(ReservaController.class)
public class ReservaControllersTeste {

    @MockBean
    private ReservaService reservaService;
    @Autowired
    private MockMvc mockMvc;

    Hospede hospede = new Hospede();
    Propriedade propriedade = new Propriedade();
    ReservaDTO reservaDTO = new ReservaDTO();
    Reserva reserva = new Reserva();

    @BeforeEach
    public void Setup() {
        hospede.setId(23);
        propriedade.setId(2);
        propriedade.setValorDiaria(200.20);


        reservaDTO.setIdPropriedade(2);
        reservaDTO.setIdHospede(2);


        reserva.setPropriedade(propriedade);
        reserva.setHospede(hospede);
        reserva.setDataInicio(LocalDate.parse("2020-09-08"));
        reserva.setDataFim(LocalDate.parse("2020-09-10"));
        reserva.setValorDaReserva(400.40);
        reserva.setId(UUID.randomUUID());

    }

    @Test
    public void testarFuncaoPost() throws Exception {
        Mockito.when(reservaService.salvarReserva(Mockito.any(ReservaDTO.class))).thenReturn(reserva);

        ObjectMapper mapper = new ObjectMapper();
        String jsonReserva = mapper.writeValueAsString(reservaDTO);
        jsonReserva.replace("\"dataInicio\":null,","\"dataInicio\":\"2020-09-08\",");
        jsonReserva.replace("\"dataFim\":null,","\"dataFim\":\"2020-09-10\",");

        mockMvc.perform(MockMvcRequestBuilders.post("/reserva")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReserva))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

}
