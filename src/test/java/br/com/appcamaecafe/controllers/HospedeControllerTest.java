package br.com.appcamaecafe.controllers;

import br.com.appcamaecafe.enums.FormaDePagamento;
import br.com.appcamaecafe.enums.Genero;
import br.com.appcamaecafe.models.Hospede;
import br.com.appcamaecafe.models.Reserva;
import br.com.appcamaecafe.services.HospedeService;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HospedeControllerTest {
    @MockBean
    private HospedeService hospedeService;
    @Autowired
    private MockMvc mockMvc;

    Hospede hospede;
    List<Reserva> reservas;

    @BeforeEach
    public void setUp(){
        reservas = new ArrayList<Reserva>();
        hospede = new Hospede();
        hospede.setId(1);
        hospede.setFormaDePagamento(FormaDePagamento.CARTAO);
        hospede.setReservas(reservas);
        hospede.setCpf("76748533066");
        hospede.setDataDeNascimento(LocalDate.parse("2000-01-01"));
        hospede.setEmail("bob@bobmail.com");
        hospede.setNome("Bob");
        hospede.setGenero(Genero.MASCULINO);
        hospede.setTelefone(123456789);
    }



}
